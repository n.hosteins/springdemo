package com.example.demo.controller;

import com.example.demo.data.MessageDOA;
import com.example.demo.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;



@Controller
public class MessageController {
    @Autowired
    private MessageDOA messageDOA;

    @GetMapping("/msg")
    public String showMessages(Model model){
        model.addAttribute("msgList", messageDOA.findAll());
        model.addAttribute("newMessage", new Message());

        return "MesgPage";
    }

   @GetMapping("/msg/{id}")
    public String updateMessageById( Model model, @PathVariable(value="id") Long messageId){
        Message msg = messageDOA.findById(messageId).orElse(null);
        model.addAttribute("messageById", msg);
        return "oneMessage";
    }

    @PostMapping("/msg")
    public String postMessage(@ModelAttribute Message newMessage){
Message msg = new Message(newMessage.getAuthor(), newMessage.getMessages());
messageDOA.save(msg);
return "redirect:/msg";
    }

    @PostMapping ("/updatemsg/{messageId}")
    public String putMessage ( @ModelAttribute @RequestBody Message updatedMessage, @PathVariable Long messageId){
        Message upmsg = messageDOA.findById(messageId).orElse(null);
        upmsg.setAuthor(updatedMessage.getAuthor());
        upmsg.setMessages(updatedMessage.getMessages());
        System.out.println("author " + updatedMessage.getAuthor());
        System.out.println("id " + upmsg.getId());
         messageDOA.save(upmsg);
        return "redirect:/msg";
    }

    @GetMapping("/delmsg/{id}")
        public String delMessage (@PathVariable Long id) {
        messageDOA.deleteById(id);
        return "redirect:/msg";
        }
}
