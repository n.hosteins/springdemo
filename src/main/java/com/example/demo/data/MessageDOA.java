package com.example.demo.data;

import com.example.demo.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public interface MessageDOA  extends JpaRepository<Message, Long>{
}
