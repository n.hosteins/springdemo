package com.example.demo.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "messagelist")
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id" )
    private Long id ;
    @Column(name = "author" )
    private String author;
    @Column(name = "time_stamp" )
    private LocalDateTime timeStamp;
    @Column(name = "message" )
    private String messages;

    public Message(String author, String messages) {
        this.author = author;
        this.messages = messages;
        this.timeStamp = LocalDateTime.now();
    }

    public Message() {
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String message) {
        this.messages = message;
    }

    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    public Long getId() {
        return id;
    }


}
